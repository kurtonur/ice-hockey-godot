extends Node


##devam Et
export(Array,int) var layers = []
export(String) var AISaveFile :String= "user://ModelName.md"
export(Vector2) var startRandomization : Vector2 = Vector2(-0.5,0.5)
export(Vector2) var learningRate :Vector2 = Vector2(-0.1,0.1)
export(float) var bias :float= 0
export(int) var bestFitness :int= 0

export(NeuralNetwork.MutationFunctions) var mutationFunction :int= NeuralNetwork.MutationFunctions.Default
export(NeuralNetwork.ActivationFuctions) var activationFuction :int = NeuralNetwork.ActivationFuctions.Default

export(float,1,50,0.1) var GameSpeed :float = 1 

onready var LabelGeneration :Label = get_node("UI/Generation")

onready var player1 = get_node("../Player1")
var player1Fitness = 0

onready var player2 = get_node("../Player2")
var player2Fitness = 0

onready var timer = get_node("Timer")

func _ready():
	Engine.time_scale = GameSpeed
	CreateGeneration()
	Signals.connect("PuckHit",self,"PlayerCollisionCheck")
	Signals.connect("Scored",self,"ScoredCheck")
	pass
	

func CheckPlayers():
	if(player1Fitness > player1.AI.GetFitness()):
		player1.AI.SetFitness(player1Fitness)
		player1.AI.SaveNetworkToFile(AISaveFile+player1.name)
	pass
	if(player2Fitness > player2.AI.GetFitness()):
		player2.AI.SetFitness(player2Fitness)
		player2.AI.SaveNetworkToFile(AISaveFile+player2.name)
	pass
	if(player1.playerScore > player2.playerScore):
		Core.winner = 1
	elif(player1.playerScore < player2.playerScore):
		Core.winner = 2
	pass

# warning-ignore:unused_argument
func _input(event):
	if Input.is_action_pressed("startTrain"):
		Core.gameStatus = Core.GameStatus.Playing
	if Input.is_action_just_pressed("GameOver"):
		Core.gameStatus = Core.GameStatus.GameOver
		infoPrint()
		CheckPlayers()
		reLoad()
	if Input.is_action_just_pressed("SaveAI"):
		player2.AI.SaveNetworkToFile(AISaveFile+player2.name)
		player1.AI.SaveNetworkToFile(AISaveFile+player1.name)
	pass
	
func CreateGeneration():
	Core.AIGeneration += 1
	LabelGeneration.text = "Generation : " + String(Core.AIGeneration)
	SetInitPlayerAI(player1)
	SetInitPlayerAI(player2)
	player1.isPlayer = false
	player2.isPlayer = false
	if(Core.winner == 1):
		player2.AI.Mutate(NeuralNetwork.MutationFunctions.MultiplyByZeroOne)
		print("PLayer 2 is  mutated")
	if(Core.winner == 2):
		player1.AI.Mutate(NeuralNetwork.MutationFunctions.MultiplyByZeroOne)
		print("PLayer 1 is  mutated")
	pass
	if(Core.winner != 1 and Core.winner!= 2):
		player2.AI.Mutate(NeuralNetwork.MutationFunctions.MultiplyByZeroOne)
		print("PLayer 1 is  mutated")
	pass

func SetInitPlayerAI(player:KinematicBody2D,isSaveFile :bool = true):
	var file2Check = File.new()
	var doFileExists = file2Check.file_exists(AISaveFile+player.name)
	player.AI.Cleaner()
	if(doFileExists && isSaveFile):
		player.AI.LoadNetworkFromFile(AISaveFile+player.name)
	else:
		player.AI.InitLayers(layers)
		player.AI.SetAllBias(bias)
		player.AI.SetLearningRate(learningRate)
		player.AI.SetBestFitness(500)
		player.AI.mutationFunction = mutationFunction
		player.AI.activationFuction = activationFuction
	return player
	pass

func _on_Timer_timeout():
	var puck = get_tree().get_nodes_in_group("Puck")
	if(puck.size()>0 and puck[0].get_linear_velocity().length() == 0):
		print("Generation ->" + String(Core.AIGeneration))
		print("Player 1 Fitness ->" + String(player1Fitness))
		print("Player 1 AI Fitness ->" + String(player1.AI.GetFitness()))
		print("Player 2 Fitness ->" + String(player2Fitness))
		print("Player 2 AI Fitness ->" + String(player2.AI.GetFitness()))
		Core.gameStatus = Core.GameStatus.GameOver
		CheckPlayers()
		reLoad()
		pass
	pass

func reLoad():
	Core.gameStatus = Core.GameStatus.GameOver
	Core.LoadingNode.ToBlack()
	yield(Core.LoadingNode.animate,"animation_finished")
	var play = SceneManager.AddNode(Core.PlayTSCN,Core.PlayNode)
	Core.gameType = Core.GameType.Player1
	play.setPlayersControll()
	Core.LoadingNode.Idle()
	get_parent().queue_free()

#	Core.gameStatus = Core.GameStatus.GameOver
#	var play = SceneManager.AddNode(("res://T/Node.tscn"))
#	get_parent().queue_free()
	
#	Core.gameType = Core.GameType.Player1
#	play.setPlayersControll()
#	Core.LoadingNode.Idle()
#	get_parent().queue_free()
	pass

func PlayerCollisionCheck(who):
	if(who.name == "Player1"):
		player1Fitness += 0
	if(who.name == "Player2"):
		player2Fitness += 0
	pass

func ScoredCheck(who):
	if(who == "player1"):
		player1Fitness += 10
		player2Fitness -= 5
	if(who == "player2"):
		player2Fitness += 10
		player1Fitness -= 5
	CheckPlayers()
	reLoad()
	infoPrint()
	pass

func infoPrint():
	print("Generation ->" + String(Core.AIGeneration))
	print("Player 1 Fitness ->" + String(player1Fitness))
	print("Player 1 AI Fitness ->" + String(player1.AI.GetFitness()))
	print("Player 2 Fitness ->" + String(player2Fitness))
	print("Player 2 AI Fitness ->" + String(player2.AI.GetFitness()))
