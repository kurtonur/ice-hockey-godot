extends RigidBody2D

onready var hitPlayer :AudioStreamPlayer = get_node("Sounds/HitPlayer") 


var velocity :Vector2 = Vector2.ZERO
var previousVelo :Vector2 = Vector2.ZERO
var speedScale :float = 0

func _ready():
	self.set_meta("Puck",true) 
	pass 

# warning-ignore:unused_argument
func _physics_process(delta):
	velocity = get_linear_velocity()
	speedScale = Vector2.ZERO.distance_to(velocity)
	if(speedScale < 3500):
		previousVelo = velocity
		set_linear_velocity(velocity)
	else:
		set_linear_velocity(previousVelo)
	pass

func setMultiPlayerJoined():
	self.mode = RigidBody2D.MODE_KINEMATIC
	pass
	
func setMultiplayerPos(center,ServerPos):
	var newPos = (center - ServerPos) + center
	#var direction = global_position.direction_to(newPos).normalized()
	var lerpMe = lerp(global_position,newPos,0.3)
	resetPosition(lerpMe)
	pass
	
func getPosition():
	return global_position
	pass

func resetPosition(Pos):
	global_position = Pos
	pass

func _on_PuckArea_body_entered(body):
	if body.has_meta("Player"):
		hitPlayer.play()
		Signals.emit_signal("PuckHit",body)
	pass 
