extends KinematicBody2D

onready var hitPlayer :AudioStreamPlayer = get_node("Sounds/HitPlayer") 

var motion :Vector2 = Vector2.ZERO
var speed :int = 950

var isMoveAndSlide :bool = true

var nextPos :Vector2 = Vector2.ZERO

func _ready():
	self.set_meta("Puck",true)
	#setIsMoveAndSlide(false,25)
	pass 

func setIsMoveAndSlide(value :bool,speed):
	self.speed  = speed
	isMoveAndSlide = false
	pass
	
func _physics_process(delta):
	if Core.gameStatus == Core.GameStatus.Playing:
		move(delta)
	pass

func move(delta):
	var Direct = global_position.direction_to(nextPos).normalized()
	if(global_position.distance_to(nextPos) > 25):
		if isMoveAndSlide:
			motion = move_and_slide(Direct*speed*delta*global_position.distance_to(nextPos),Vector2.ZERO)
		else:
			move_and_collide(Direct*speed*delta*global_position.distance_to(nextPos))
	pass

func setMultiplayerPos(center,ServerPos):
	var newPos = (center - ServerPos) + center
	setNextPos(newPos)
	pass

func setNextPos(Pos):
	nextPos = Pos
	pass

func getPosition():
	return global_position
	pass

func resetPosition(Pos):
	global_position = Pos
	pass

func _on_PuckArea_body_entered(body):
	if body.has_meta("Player"):
		
		hitPlayer.play()
	pass 
