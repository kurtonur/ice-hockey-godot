extends Node2D

onready var player1Pos :Position2D = get_node("Player1")
onready var player2Pos :Position2D = get_node("Player2")
onready var puck :Position2D = get_node("Puck")
onready var scoredParticle :Particles2D = get_node("ScoredParticle")

func _ready():
	pass

func getPlayer1Pos():
	return player1Pos.global_position
	pass
	
func getPlayer2Pos():
	return player2Pos.global_position
	pass

func getPuckPos():
	return puck.global_position
	pass

func _on_Player1Goal_body_entered(body):
	if body.has_meta("Puck"):
		scoredParticle.emitting = true
		Signals.emit_signal("Scored","player1")
	pass 

func _on_Player2Goal_body_entered(body):
	if body.has_meta("Puck"):
		scoredParticle.emitting = true
		Signals.emit_signal("Scored","player2")
	pass
