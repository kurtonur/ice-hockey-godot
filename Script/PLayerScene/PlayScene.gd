extends Node

onready var field :Node2D = get_node("Field")
onready var player1 :KinematicBody2D = get_node("Player1")
onready var player2 :KinematicBody2D = get_node("Player2")
onready var puck :RigidBody2D = get_node("Puck")
onready var scoredSound :AudioStreamPlayer = get_node("Sounds/Scored")
onready var timerMultiPlayerPosSender :Timer = get_node("SendMultiplayerPos") 

func _ready():
	
	setStartPos()
	# warning-ignore:return_value_discarded
	Signals.connect("Scored",self,"Scored")
# warning-ignore:return_value_discarded
	Multiplayer.connect("ServerUpdate",self,"ServerUpdate")
	pass

func setStartPos():
	player1.resetPLayer(field.getPlayer1Pos())
	player2.resetPLayer(field.getPlayer2Pos())
	puck.resetPosition(field.getPuckPos())
	
	pass

func Scored(player):
	scoredSound.play()
	if(player == "player1"):
		player1.myScorePlus1()
	if(player == "player2"):
		player2.myScorePlus1()
	player1.resetPLayer(field.getPlayer1Pos())
	player2.resetPLayer(field.getPlayer2Pos())
	ReloadPuck()
	if player1.getMyScore() >= Core.maxScoreForGameOver or player2.getMyScore()>=Core.maxScoreForGameOver:
		Core.gameStatus = Core.GameStatus.GameOver
	pass

func setPlayersControll():
	if Core.gameType == Core.GameType.Player1:
		player2.isPlayer = false
	elif Core.gameType == Core.GameType.Player2:
		player2.isPlayer = true
	elif Core.gameType == Core.GameType.MultiplayerCreate:
		player2.isPlayer = false
	elif Core.gameType == Core.GameType.MultiplayerJoin:
		player1.playerSC = player1.playerSpriteColors.Blue
		player1.setPlayerColor()
		player2.playerSC = player2.playerSpriteColors.Red
		player2.setPlayerColor()
		player2.isPlayer = false
		puck.setMultiPlayerJoined()
	pass

func ReloadPuck():
	puck.queue_free()
	puck = load("res://Object/Puck/Puck.tscn").instance()
	puck.resetPosition(field.getPuckPos())
	puck.name = "Puck"
	if Core.gameType == Core.GameType.MultiplayerJoin:
		puck.setMultiPlayerJoined()
	add_child(puck)
	pass

func setTimerMultiPlayerPosSender():
	if Core.gameType == Core.GameType.MultiplayerCreate || Core.gameType == Core.GameType.MultiplayerJoin:
		timerMultiPlayerPosSender.start()
	pass

func _on_StartToPlay_timeout():
	Core.gameStatus =Core.GameStatus.Playing
	player1.clickedPos = field.getPlayer1Pos()
	player2.clickedPos = field.getPlayer2Pos()
	setTimerMultiPlayerPosSender()
	pass 

func _on_SendMultiplayerPos_timeout():
	if Core.gameType == Core.GameType.MultiplayerCreate:
		Multiplayer.sendPlayerPos(player1.clickedPos,puck.getPosition())
	elif Core.gameType == Core.GameType.MultiplayerJoin:
		Multiplayer.sendPlayerPos(player1.clickedPos)
	pass 

# warning-ignore:shadowed_variable
func ServerUpdate(players,puck):
	for key in players.keys():
		if key != Multiplayer.connectionInfo.sid && key != "puck":
			player2.setMultiplayerPos(field.getPuckPos(),Vector2(players[key].position.x,players[key].position.y))
	if puck != null and Core.gameType == Core.GameType.MultiplayerJoin:
		self.puck.setMultiplayerPos(field.getPuckPos(),Vector2(puck.position.x,puck.position.y))
	pass
