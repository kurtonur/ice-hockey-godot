extends Node

onready var player1Score :Label = get_node("Canvas/Player1Score")
onready var player2Score :Label = get_node("Canvas/Player2Score")
onready var anim :AnimationPlayer = get_node("Canvas/Animate")

onready var player1 :KinematicBody2D = get_parent().get_node("Player1")
onready var player2 :KinematicBody2D = get_parent().get_node("Player2")

onready var backButton :Button = get_node("Canvas/Back")

onready var gameOver :Label = get_node("Canvas2/GameOver")

func _ready():
# warning-ignore:return_value_discarded
	Signals.connect("Scored",self,"ScoreChange")
	pass
	
func ScoreChange(player):
	if(player == "player1"):
		anim.play("Player1Scored")
		player1Score.text = String(player1.getMyScore())
	if(player == "player2"):
		anim.play("Player2Scored")
		player2Score.text = String(player2.getMyScore())
	if player2.isTraining and player1.getMyScore() >= 7 or player2.getMyScore()>=7:
		gameOver.show()
	pass

func _on_Button_pressed():
	Core.gameStatus = Core.GameStatus.Menu
	backButton.disabled = true
	Core.LoadingNode.ToBlack()
	if Multiplayer.isConnected:
		Multiplayer.QuitRoom()
	yield(Core.LoadingNode.animate,"animation_finished")
	SceneManager.AddNode(Core.MenuTSCN,Core.MenuNode)
	Core.LoadingNode.Idle()
	SceneManager.RemoveNodeChildren(Core.PlayNode)
	pass 
