extends KinematicBody2D

var playerSpriteRed :Resource = load("res://Assets/Image/PlayerRed.png")
var PlayerSpriteBlue :Resource = load("res://Assets/Image/PlayerBlue.png")
enum playerSpriteColors {Red, Blue}

export(bool) var isPlayer = false

export(playerSpriteColors) var playerSC = playerSpriteColors.Red
onready var sprite :Sprite = get_node("Sprite")

var clickedPos :Vector2 = Vector2.ZERO
var playerClicked :int = -1
var clickIndex :int = -2

var motion :Vector2 = Vector2.ZERO
var speed :int = 950

var isMoveAndSlide = true

var playerScore :int = 0

onready var AI = get_node("AI")

var isTraining :bool = false

func _ready():
	self.set_meta("Player",true)
	setPlayerColor()
	#setIsMoveAndSlide(false,25)
	AI.LoadNetworkFromFile("user://test.mdPlayer2")
	pass 

func setPlayerColor():
	if playerSC == playerSpriteColors.Red:
		sprite.texture = playerSpriteRed
	else:
		sprite.texture = PlayerSpriteBlue
	pass
	
func setIsMoveAndSlide(value :bool,speed):
	self.speed  = speed
	isMoveAndSlide = false
	pass
	
func _physics_process(delta):
	if Core.gameStatus == Core.GameStatus.Playing:
		if !isPlayer:
			AImove(delta)
		else:
			move(delta)
			pass
	pass

func _input(event):
	if isPlayer and playerClicked == clickIndex and (event is InputEventScreenDrag) and playerClicked == event.index:
		clickedPos = event.position
	if isPlayer and playerClicked ==  clickIndex and (event is InputEventScreenTouch and !event.is_pressed()) and playerClicked == event.index:
		playerClicked = -1
		clickIndex = -2
		pass
	pass
	
# warning-ignore:unused_argument
func _on_Player_input_event(viewport, event, shape_idx):
	if isPlayer and playerClicked == -1 and (event is InputEventScreenTouch and event.is_pressed()):
		clickIndex = event.index
		playerClicked = clickIndex
	pass 

func move(delta):
	var Direct = global_position.direction_to(clickedPos).normalized()
	if(global_position.distance_to(clickedPos) > 25):
		if isMoveAndSlide:
			motion = move_and_slide(Direct*speed*delta*global_position.distance_to(clickedPos),Vector2.ZERO)
		else:
			move_and_collide(Direct*speed*delta*global_position.distance_to(clickedPos))
	pass

func AImove(delta):
	var ForwardValue = AI.Forward(getDataForAI())
	var direction :Vector2 = Vector2(ForwardValue[0]-ForwardValue[1],ForwardValue[2]-ForwardValue[3]).normalized()
	if isMoveAndSlide:
		motion = move_and_slide(direction*speed*delta*100,Vector2.ZERO)
	else:
		move_and_collide(direction*speed*delta*100)
	pass
	
func getDataForAI():
	var players = get_tree().get_nodes_in_group("Player")
	var puck = get_tree().get_nodes_in_group("Puck")
	var data :Array = Array()
	
	#data.append(global_position.x)
	#data.append(global_position.y)
	
	if(puck.size()> 0):
		var pos :Vector2= puck[0].global_position
		var direct :Vector2 = puck[0].get_linear_velocity().normalized()
		var distance :Vector2 = global_position - pos
		#data.append(pos.x)
		#data.append(pos.y)
		data.append(distance.x)
		data.append(distance.y)
	else:
		data.append(0)
		data.append(0)
		pass
#	for player  in players:
#		if (player.name != name):
#			data.append(player.global_position.x)
#			data.append(player.global_position.y)
#		pass
	return data
	pass
	
func setMultiplayerPos(center,ServerPos):
	var newPos = (center - ServerPos) + center
	setClickedPos(newPos)
	pass

func setClickedPos(Pos):
	clickedPos = Pos
	pass

func resetPLayer(Pos = Vector2.ZERO):
	self.global_position = Pos
	clickedPos = Pos
	playerClicked = -1
	clickIndex = -2
	pass

func getMyScore():
	return playerScore
	pass
	
func myScorePlus1():
	playerScore += 1
	pass
