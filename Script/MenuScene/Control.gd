extends Control

onready var animate :AnimationPlayer = get_node("Animate")

onready var player1 :Button = get_node("Menu1/1player")
onready var player2 :Button = get_node("Menu1/2player")
onready var multi :Button = get_node("Menu1/Multi")

onready var createServer :Button= get_node("Menu2/CreateServer")
onready var joinServer :Button= get_node("Menu2/JoinServer")

onready var createServerName :TextEdit = get_node("Menu4/CreateServerName")

onready var m3JoinServer :Button = get_node("Menu3/M3JoinServer")
onready var joinServerName :TextEdit = get_node("Menu3/JoinServerName")


func _ready():
# warning-ignore:return_value_discarded
	Multiplayer.connect("RoomInfo",self,"RoomInfo")
	pass
	
func _on_1player_pressed():
	player1.disabled = true
	Core.LoadingNode.ToBlack()
	yield(Core.LoadingNode.animate,"animation_finished")
	var play = SceneManager.AddNode(Core.PlayTSCN,Core.PlayNode)
	Core.gameType = Core.GameType.Player1
	play.setPlayersControll()
	Core.LoadingNode.Idle()
	SceneManager.RemoveNodeChildren(Core.MenuNode)
	pass

func _on_2player_pressed():
	player2.disabled = true
	Core.LoadingNode.ToBlack()
	yield(Core.LoadingNode.animate,"animation_finished")
	var play = SceneManager.AddNode(Core.PlayTSCN,Core.PlayNode)
	Core.gameType = Core.GameType.Player2
	play.setPlayersControll()
	Core.LoadingNode.Idle()
	SceneManager.RemoveNodeChildren(Core.MenuNode)
	pass 

func _on_Multi_pressed():
	multi.disabled = true
	if !Multiplayer.isConnected:
		Multiplayer.ServerConnect()
		yield(Multiplayer.client,"connection_established")
	animate.play("MultiPlayer")
	multi.disabled = false
	pass

func _on_MultiBack_pressed():
	animate.play("MultiPlayerBack")
	pass 

func _on_JoinServer_pressed():
	animate.play("MultiplayerJoin")
	pass
	
func _on_JoinBack_pressed():
	animate.play("MultiplayerJoinBack")
	m3JoinServer.disabled = false
	pass

func _on_CreateServer_pressed():
	createServer.disabled = true
	joinServer.disabled = true
	Multiplayer.CreateRoom(OS.get_unique_id())
	pass 
	
# warning-ignore:unused_argument
func RoomInfo(roomName,players):
	if players.size() == 2:
		createServerName.text = String(roomName)
		animate.play("MultiplayerCreate")
		createServer.disabled = false
		joinServer.disabled = false
		Core.gameType = Core.GameType.MultiplayerCreate
	elif players.size() == 3:
		player2.disabled = true
		Core.LoadingNode.ToBlack()
		yield(Core.LoadingNode.animate,"animation_finished")
		var play = SceneManager.AddNode(Core.PlayTSCN,Core.PlayNode)
		if Core.gameType != Core.GameType.MultiplayerCreate:
			Core.gameType = Core.GameType.MultiplayerJoin
		play.setPlayersControll()
		Core.LoadingNode.Idle()
		SceneManager.RemoveNodeChildren(Core.MenuNode)
		pass
	pass

func _on_CreateBack_pressed():
	Multiplayer.QuitRoom()
	animate.play("MultiplayerCreateBack")
	pass

func _on_ServerShare_pressed():
	var serverName = createServerName.text
	Core.ShareInfo("Ice Hockey","Sharing Server Code", "Server -> " + serverName)
	pass 

func _on_M3JoinServer_pressed():
	m3JoinServer.disabled = false
	if joinServerName.text != "" :
		Multiplayer.JoinRoom(joinServerName.text)
	m3JoinServer.disabled = true
	pass 
