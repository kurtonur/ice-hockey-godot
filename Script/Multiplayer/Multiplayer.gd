extends Node

var serverAdress :String = "wss://airhockeyserver.herokuapp.com/socket.io/?EIO=3&transport=websocket"

var client :WebSocketClient = WebSocketClient.new()

var isConnected :bool = false

signal RoomInfo(data,size)
# warning-ignore:unused_signal
signal ServerUpdate(players,puck)

var timer :Timer = Timer.new()
var connectionInfo :Dictionary = {}

func _ready():
	
# warning-ignore:return_value_discarded
	client.connect("connection_error", self, "Error")
# warning-ignore:return_value_discarded
	client.connect("connection_closed", self, "Closed")
# warning-ignore:return_value_discarded
	client.connect("connection_established", self, "Connected")
# warning-ignore:return_value_discarded
	client.connect("data_received", self, "getdata")
	
	timer.name = "KeepMeInServer"
	timer.name = "KeepMeInServer"
	timer.wait_time = 5
	timer.one_shot = false
	timer.autostart = false
	add_child(timer)
# warning-ignore:return_value_discarded
	timer.connect("timeout",self,"KeepMeInServer")
	
	pass

# warning-ignore:unused_argument
func _process(delta):
	client.poll()
	pass

func ServerConnect():
	var err = client.connect_to_url(serverAdress)
	if err != OK:
		set_process(false)
		isConnected = false 
	else:
		set_process(true)
		isConnected = true
	return isConnected
	pass

func ServerQuit():
	client.disconnect_from_host()
	isConnected = false
	pass

func Error():
	print_debug("Server connection is ERROR")
	timer.stop()
	set_process(false)
	isConnected = false
	pass

# warning-ignore:unused_argument
func Closed(was_clean = false):
	print_debug("Server connection is CLOSED")
	timer.stop()
	set_process(false)
	isConnected = false
	pass

# warning-ignore:unused_argument
func Connected(proto = ""):
	print_debug("Server connection is SUCCESS")
	client.get_peer(1).set_write_mode(WebSocketPeer.WRITE_MODE_TEXT)
	timer.start()
	isConnected = true
	pass

func getdata():
	var rawData = client.get_peer(1).get_packet().get_string_from_utf8()
	var data = _rawDataToDictionary(rawData)
	if rawData[0] == "0":
		setConnectionInfo(data)
	elif rawData[0] == "4":
		getRoomInfo(data)
		getPosUpdates(data)
		pass
	pass

func _rawDataToDictionary(var _rawText):
	var find1 = _rawText.find("[")
	var find2 = _rawText.find("{")
	if find1>-1 || find2>-1:
		if find1>-1 && (find2 == -1 || find1<find2):
			var _splitTextArray = _rawText.split("[");
			var _jsonString = str("[",_splitTextArray[1])
			var _data = parse_json(_jsonString);
			return _data
		else:
			var _splitTextArray = _rawText.split("{");
			var _jsonString = str("{",_splitTextArray[1])
			var _data = parse_json(_jsonString);
			return _data
	return _rawText

func setConnectionInfo(data):
	connectionInfo = data
	print_debug(connectionInfo)
	timer.wait_time = (data.pingTimeout /1000) - 1
	pass

func getRoomInfo(data):
	if data is Array && data[0].to_lower() == "room":
		print_debug(data)
		var players :Dictionary = data[1].playersInRoom
		var roomName  = data[1].roomName
		emit_signal("RoomInfo",roomName,players)
		pass
	pass

func sendPlayerPos(PlayerPos,PuckPos = null):
	var sendingData = ""
	if PuckPos != null:
		sendingData = '42["update",{"player":{"position":{"x":'+String(PlayerPos.x)+', "y":'+String(PlayerPos.y)+'}},"puck":{"position":{"x":'+String(PuckPos.x)+', "y":'+String(PuckPos.y)+'}}}]'
	else:
		sendingData = '42["update",{"player":{"position":{"x":'+String(PlayerPos.x)+', "y":'+String(PlayerPos.y)+'}}}]'
		pass
# warning-ignore:return_value_discarded
	client.get_peer(1).put_packet(sendingData.to_utf8())
	pass

func getPosUpdates(data):
	if data is Array && data[0].to_lower() == "update":
		var players :Dictionary = data[1].playersInRoom
		var puck = null
		if data[1].has("puck"):
			puck = data[1].puck
		emit_signal("ServerUpdate",players,puck)
		pass
	pass

func KeepMeInServer():
# warning-ignore:return_value_discarded
	client.get_peer(1).put_packet("2".to_utf8())
	pass

func JoinRoom(roomName):
	var temp = '42["joinRoom","'+roomName+'"]'
# warning-ignore:return_value_discarded
	client.get_peer(1).put_packet(temp.to_utf8())
	pass

func CreateRoom(roomName):
	var temp = '42["createRoom","'+roomName+'"]'
# warning-ignore:return_value_discarded
	client.get_peer(1).put_packet(temp.to_utf8())
	pass

func QuitRoom():
	var temp = '42["leaveRoom","left"]'
# warning-ignore:return_value_discarded
	client.get_peer(1).put_packet(temp.to_utf8())
	pass
