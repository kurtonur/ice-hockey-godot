extends Node

onready var player2 :KinematicBody2D = get_parent().get_node("Player2") 
onready var puck :RigidBody2D = get_parent().get_node("Puck")

var isIAOn :bool = false

func CheckPlayer2():
	if !player2.isPlayer:
		isIAOn = true
	else:
		isIAOn = false
		set_process(isIAOn)
	pass

# warning-ignore:unused_argument
func _process(delta):
	if isIAOn:
		player2.clickedPos = puck.global_position
		pass
	pass
