extends Node

func AddNode(addingNodePath,gameNode = Core.GameNode,nameNode = ""):
	var newActiveNode = load(addingNodePath).instance()
	if nameNode != "":
		newActiveNode.name = nameNode
		pass
	gameNode.add_child(newActiveNode)
	return newActiveNode
	pass
	
func AddScript(addingNodePath,gameNode = Core.gameNode):
	var newActiveNode = load(addingNodePath).new()
	gameNode.add_child(newActiveNode)
	return newActiveNode
	pass

func RemoveNode(node):
	node.queue_free()
	pass
	
func ChangeNode(new,exist):
	exist.replace_by(new)
	pass

func RemoveNodeChildren(node):
	for n in node.get_children():
		n.queue_free()
	pass
