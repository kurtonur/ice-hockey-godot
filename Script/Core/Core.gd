extends Node

onready var GameNode :Node = get_tree().get_root().get_node("Game")
onready var PlayNode :Node = GameNode.get_node("Play")
onready var MenuNode :Node = GameNode.get_node("Menu")
onready var LoadingNode :Node = GameNode.get_node("Loading")

var PlayTSCN :String = "res://Scene/Play.tscn"
var MenuTSCN :String = "res://Scene/Menu.tscn"

enum GameType {Player1,Player2,MultiplayerCreate,MultiplayerJoin,TrainAI}
enum GameStatus {Menu,Loading,Playing,GameOver}

var gameStatus = GameStatus.Menu
var gameType = GameType.Player1
const maxScoreForGameOver = 1000

var winner :int = 0 
var AIGeneration = 0

func ShareInfo(title,subject,text):
	var share = Engine.get_singleton("GodotShare")
	if share != null:
		share.shareText(title, subject, text)
	pass
