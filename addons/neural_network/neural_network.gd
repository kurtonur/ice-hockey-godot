tool
extends EditorPlugin

onready var interface = get_editor_interface()
onready var selection = interface.get_selection()

var panel :Control

var new = NeuralNetwork.new()

func _enter_tree():
	assert(Engine.get_version_info().major >= 3)
	print("Neural Network Plugin Activated")
	panel = preload("res://addons/neural_network/Editor/Interface.tscn").instance()
	add_control_to_bottom_panel(panel,"Neural Networks")
	add_custom_type("NeuralNetwork","Node",preload("res://addons/neural_network/Scripts/NeuralNetwork.gd"),preload("res://addons/neural_network/icon.svg"))
	add_autoload_singleton("NN_Debuger","res://addons/neural_network/runtime/NN_Debuger.gd")
	new.Cleaner()
	new.InitLayers([2,2,1])
	panel.initNetwork(new)
	pass

func _process(delta):
	interface = get_editor_interface()
	selection = interface.get_selection()
	if  interface.is_playing_scene() and OS.is_debug_build():
		
		pass
	return

func edit(object : Object):
	pass
	
#func handles(object : Object):
#
#	for item in get_editor_interface().get_selection().get_selected_nodes():
#		if(item as NeuralNetwork):
#			#if(selectedNeuralNodes.find(item) == -1):
#			#	selectedNeuralNodes.append(item)
#				#neurals.Forward([randf(),randf()])
#				#panel.SetNeuralValues(item)
#			pass
#	#panel.AddtoList(selectedNeuralNodes)
#	pass
	
func _exit_tree():
	print("Neural Network Plugin Deactivated")
	remove_control_from_bottom_panel(panel)
	remove_autoload_singleton("NN_Debuger")
	remove_custom_type("NeuralNetwork")
	pass

func get_plugin_icon():
	return get_editor_interface().get_base_control().get_icon("GraphNode", "EditorIcons")

func get_plugin_name():
	return "NeuralNetwork"
	pass
