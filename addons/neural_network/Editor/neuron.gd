tool
extends Panel

var weight :Array = Array()
var wieghtLine :Array = Array() 
var value :float = 0
var isActive = true
var baseColor :Color = Color.white
var disableColor :Color = Color.navyblue


func SetValue(value :float) ->void:
	if(value):
		self.value = value
		get_node("Value").text =  String(stepify(value,0.0001))
	pass

func AddNewWeight(weight:float ,pos:Vector2):
	self.weight.append(weight)
	var newLine :Line2D = Line2D.new()
	newLine.add_point(Vector2.ZERO)
	newLine.add_point(pos)
	newLine.default_color = Color(rand_range(0,abs(weight)),rand_range(abs(weight),1),rand_range(0,1))
	newLine.default_color.a = 0.2
	newLine.end_cap_mode =Line2D.LINE_CAP_ROUND
	newLine.begin_cap_mode =Line2D.LINE_CAP_ROUND
	get_node("Weights").add_child(newLine)
	wieghtLine.append(newLine)
	pass

func ChangeColor(color:Color):
	#set("custom_styles/panel/bg_color",color)
	baseColor = color
	modulate = color
	pass

func _gui_input(event):
	if event is InputEventMouseButton and event.pressed:
		if(isActive):
			isActive = false
			pass
		else:
			isActive = true
			pass
	CheckClick()
	pass

func SetDisabled(value:bool = false):
	self.isActive = value
	CheckClick()
	pass

func CheckClick():
	if(isActive):
		modulate = baseColor
		pass
	else:
		modulate = disableColor
		pass
	pass
