tool
extends Panel


var neuronFile = load("res://addons/neural_network/Editor/neuron.tscn")

var weight :Array = Array()
var neurons :Array = Array()

onready var addNeurons = get_node("AddNeurons")


func GetNeuron(id :int) -> Panel:
	if(neurons.size() >= id):
		return neurons[id]
	else:
		return null

func SetNeuron(id:int,value:float) ->void:
	if(neurons.size()>= id):
		neurons[id].SetValue(value)
	pass

func AddNeuron(value :float,color :Color = Color.white) ->void:
	var neuronIns :Panel= neuronFile.instance()
	neuronIns.SetValue(value)
	neuronIns.ChangeColor(color)
	neurons.append(neuronIns)
	addNeurons.add_child(neuronIns)
	pass
