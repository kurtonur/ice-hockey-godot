extends Node

var server = WebSocketServer.new()
var objects = []
var connected_id = -1
var queue = []
var send_tree = false
var selected_instance

var paused = false
var step = false

#func _ready():
#	get_tree().set_meta("BT_SERVER", self)
#	if  not OS.is_debug_build():
#		print("BT release build")
#		return
#	print("BT debug build")
#	server = WebSocketServer.new()
#	server.listen(8888)
#	server.connect("client_connected", self, "debug_attached")
#	server.connect("client_disconnected", self, "debug_detached")
#	server.connect("data_received", self, "client_data")
#	print("BT debug server init")
#	return
#
#func _exit_tree():
#	if  server && connected_id != -1:
#		server.get_peer(connected_id).close(0, "")
#		for i in range(120):
#			server.poll()
#	return
