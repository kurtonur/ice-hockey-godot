extends Node
class_name NeuralNetwork ,"res://addons/neural_network/icon.svg"

enum MutationFunctions {NewRandomByLearningRate,MultiplyByTwo,MultiplyByLearningRate,MultiplyByZeroOne,MultiplyByZeroOnePlus,Custom,Default}
enum ActivationFuctions {Sigmoid,SigmoidDerivative,Tanh,TanhDerivative,ReLU,ReLUDerivative,ELU,ELUDerivative,Linear,LinearDerivative,Custom,Default}

export(Array,int) var layers :Array = Array()

var neurals:Array = Array()
var weights:Array = Array()
var bias :Array = Array()
var neuronActions :Array = Array()
var isReady :bool = false

export(float) var fitness :float = 0
export(float) var bestFitness :float = 0
export(Vector2) var startRandomization : Vector2 = Vector2(-0.5,0.5)
export(Vector2) var learningRate :Vector2 = Vector2(-0.1,0.1)

export(MutationFunctions) var mutationFunction :int= MutationFunctions.Default
export(ActivationFuctions) var activationFuction :int = ActivationFuctions.Default

func _ready():
	randomize()
	pass
	
func _enter_tree():
	self.InitLayers()
	pass
	
func GetFitness() -> float:
	return fitness
	
func GetBestFitness() -> float:
	return bestFitness

func SetFitness(value :float) -> void:
	self.fitness = value

func SetBestFitness(value :float) -> void:
	self.bestFitness = value

func GetBias() -> Array:
	return bias
	
func GetAllNeuronsAction() ->Array:
	return self.neuronActions

# warning-ignore:shadowed_variable
func SetBias(layer:int,neuron:int,bias:float) -> void:
	if(layer < layers.size()):
		if neuron < neurals[layer].size():
			self.bias[layer][neuron] = bias
			
# warning-ignore:shadowed_variable
func SetAllBias(bias :float) -> void:
	for layer in self.layers.size():
		for neuron in self.layers[layer]:
			self.bias[layer][neuron] = bias
			pass

func SetNeuronAction(layer:int,neuron:int,value:bool = true) -> void:
	if(layer < layers.size()):
		if neuron < neurals[layer].size():
			self.neuronActions[layer][neuron] = value
			
# warning-ignore:shadowed_variable
func SetAllNeuronsAction(value :bool = true) -> void:
	for layer in self.layers.size():
		for neuron in self.layers[layer]:
			self.neuronActions[layer][neuron] = value
			pass

func SetLearningRate(rate :Vector2) -> void:
	self.learningRate = rate
	pass
	
func GetLearningRate() -> Vector2:
	return self.learningRate
	
func SetStartRandomization(random :Vector2) -> void:
	self.startRandomization = random
	pass
	
func GetStartRandomization() -> Vector2:
	return self.startRandomization

func GetIsReady() -> bool:
	return self.isReady
	
# warning-ignore:shadowed_variable
func InitLayers(layers:Array = self.layers) -> void:
	if !isReady:
		if self.layers.size() <= 0:
			for item in layers:
				self.layers.append(item)
		_InitNeurals()
		_InitWeight()
		self.isReady = true
	pass

func _InitNeurals() -> void:
	for layer in range(self.layers.size()):
		self.neurals.append(Array())
		self.bias.append(Array())
		self.neuronActions.append(Array())
		for neuron in layers[layer]:
			self.neurals[layer].append(0)
			self.bias[layer].append(0)
			self.neuronActions[layer].append(true)
			pass
	pass

func _InitWeight() -> void:
	weights.append(-1)
	for layer in range(1,self.layers.size()):
		weights.append(Array())
		for neuron in neurals[layer].size():
			weights[layer].append(Array())
			for neuronWeight in layers[layer-1]:
				weights[layer][neuron].append(rand_range(startRandomization.x,startRandomization.y))
				pass
			pass
	pass

func Forward(input :Array,CustomFunction:FuncRef = null) -> Array:
	if(self.isReady):
		for layer in range(1,self.layers.size()):
			if(input.size() == layers[0]):
				neurals[0] = input
				for neuron in neurals[layer].size():
					var value = bias[layer][neuron]
					if(!self.neuronActions[layer][neuron]):
						continue
					# Edit for other Activation Functions
					for neuronWeight in layers[layer-1]:
						value += weights[layer][neuron][neuronWeight] * neurals[layer-1][neuronWeight]
						pass
						neurals[layer][neuron] = _ActivationFuction(value,activationFuction,CustomFunction)
					pass
			else:
				print("!!! Wrong input size for Neural Network !!!")
		return neurals.back()
	else:
		print("Neural Network is not ready.")
	return [-1]
	pass

func _ActivationFuction(value :float, ActivationType :int = activationFuction, CustomFunction:FuncRef = null) -> float:
	var result :float = 0
	match(ActivationType):
		ActivationFuctions.Sigmoid:
			result = 1.0 / (1.0 + exp(-value))
		ActivationFuctions.SigmoidDerivative:
			result = (1.0 / (1.0 + exp(-value))) * (1 - (1.0 / (1.0 + exp(-value))))
		ActivationFuctions.Tanh:
			result = tanh(value)
		ActivationFuctions.TanhDerivative:
			result = 1 - pow(tanh(value),2)
		ActivationFuctions.ReLUDerivative:
			if value > 0:
				result = 1
			else:
				result = 0
		ActivationFuctions.ReLU:
			result = max(0,value)
		ActivationFuctions.ELU:
			if value >= 0:
				result = value
			else:
				result = exp(value)-1
		ActivationFuctions.ELUDerivative:
			if value >= 0:
				result = 1
			else:
				result = exp(value)
		ActivationFuctions.Linear:
			result = value
		ActivationFuctions.LinearDerivative:
			result = 1
		ActivationFuctions.Custom:
			if (CustomFunction):
				result = CustomFunction.call_func(value)
		ActivationFuctions.Default:
			result = 1.0 / (1.0 + exp(-value))
	return result
	pass


func Mutate(MutationFunction:int = mutationFunction,CustomFunction:FuncRef = null) -> void:
	if(self.isReady):
		for layer in range(1,weights.size()):
			for neuron in range(weights[layer].size()):
				for neuronWeight in range(weights[layer][neuron].size()):
					var wg = weights[layer][neuron][neuronWeight]
					self.weights[layer][neuron][neuronWeight] = _MutateFunction(wg,MutationFunction,CustomFunction)
	pass

func _MutateFunction(weight :float,MutationType :int,CustomFunction:FuncRef = null) -> float:
	match(MutationType):
		MutationFunctions.MultiplyByTwo:
			weight*=2
		MutationFunctions.MultiplyByLearningRate:
			weight *= rand_range(learningRate.x,learningRate.y)
		MutationFunctions.NewRandomByLearningRate:
			weight = rand_range(learningRate.x,learningRate.y)
		MutationFunctions.MultiplyByZeroOne:
			weight *= rand_range(0.0,1.0)
		MutationFunctions.MultiplyByZeroOnePlus:
			weight *= (rand_range(0.0,1.0)+1.0)
		MutationFunctions.Custom:
			if (CustomFunction):
				weight = CustomFunction.call_func(weight)
		MutationFunctions.Default:
			weight = rand_range(learningRate.x,learningRate.y)
	return weight
	pass

#### Save Network #####

func SaveNetworkToFile(path :String) -> void:
	var data = GetNetworkData()
	var jsonData = to_json(data)
	var file = File.new()
	file.open(path, File.WRITE)
	file.store_line(jsonData)
	file.close()
	pass

func LoadNetworkFromFile(var path :String) -> void:
	var file = File.new()
	var doFileExists = file.file_exists(path)
	if(doFileExists):
		file.open(path, File.READ)
		var dataJson = file.get_line()
		file.close()
		var data = parse_json(dataJson)
		SetDataToNetwork(data)
	self.isReady = true
	pass

func GetNetworkData() -> Dictionary:
	var data :Dictionary= {
		layers = self.layers,
		neurals = self.neurals,
		weights = self.weights,
		bias  = self.bias,
		neuronActions = self.neuronActions,
		fitness = self.fitness,
		bestFitness = self.bestFitness,
		startRandomization = self.startRandomization,
		learningRate  = self.learningRate,
		mutationFunction = self.mutationFunction,
		activationFuction  = self.activationFuction,
		isReady = self.isReady
	}
	return data
	pass
	
func SetDataToNetwork(data :Dictionary) -> void:
	if data.layers:
		self.layers = data.layers
	if data.neurals:
		self.neurals = data.neurals
	if data.weights:
		self.weights = data.weights
	if data.bias:
		self.bias = data.bias
	if data.neuronActions:
		self.neuronActions = data.neuronActions
	if data.fitness:
		self.fitness = data.fitness
	if data.bestFitness:
		self.bestFitness = data.bestFitness
	if data.startRandomization:
		self.startRandomization = _StringToVector2(data.startRandomization)
	if data.learningRate:
		self.learningRate = _StringToVector2(data.learningRate)
	if data.mutationFunction:
		self.mutationFunction = data.mutationFunction
	if data.activationFuction:
		self.activationFuction = data.activationFuction
	if data.isReady:
		self.isReady = data.isReady
	pass
	
func _StringToVector2(string :String= "") -> Vector2:
	if string:
		var new_string: String = string
		new_string.erase(0, 1)
		new_string.erase(new_string.length() - 1, 1)
		var array: Array = new_string.split(", ")
		return Vector2(array[0], array[1])
	return Vector2.ZERO

func Cleaner():
	self.layers  = Array()
	self.neurals = Array()
	self.weights = Array()
	self.bias  = Array()
	self.neuronActions = Array()
	self.fitness  = 0
	self.bestFitness  = 0
	self.startRandomization  = Vector2(-0.5,0.5)
	self.learningRate  = Vector2(-0.1,0.1)
	self.mutationFunction = MutationFunctions.Default
	self.activationFuction  = ActivationFuctions.Default
	self.isReady = false
	pass

func CopyFrom(neuralNetwork :NeuralNetwork ):
	Cleaner()
	SetDataToNetwork(neuralNetwork.GetNetworkToData())
	pass
