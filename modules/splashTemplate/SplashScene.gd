extends Node

export(NodePath) var gameNode
export(String, FILE, "*.tscn") var afterSplashScene

var startProgressBar = false

func _ready():
	$splashCanvas/logoAnimation.play("fadeIn")
	pass

# warning-ignore:unused_argument
func _process(delta):
	if($splashCanvas/ProgressBar.value == $splashCanvas/ProgressBar.max_value):
		getReadyForGame()
	if(startProgressBar):
		$splashCanvas/ProgressBar.value+=2
	pass


func getReadyForGame():
	var afterSplash = load(afterSplashScene).instance()
	get_node(gameNode).add_child(afterSplash)
	queue_free()
	pass

func setStartProgressBarTrue():
	startProgressBar = true
	pass
